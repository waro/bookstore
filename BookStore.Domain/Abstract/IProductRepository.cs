﻿using BookStore.Domain.Entities;
using System.Linq;

namespace BookStore.Domain.Abstract
{
    public interface IProductRepository
    {
        IQueryable<Book> Books { get; }
        IQueryable<User> Users { get; }
        void SaveProduct(Book product);
        void SaveUser(User user);
        Book DeleteProduct(int bookID);
        User DeleteUser(int userID);
    }
}
