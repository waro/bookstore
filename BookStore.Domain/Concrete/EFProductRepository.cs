﻿using BookStore.Domain.Abstract;
using BookStore.Domain.Entities;
using System.Linq;

namespace BookStore.Domain.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Book> Books
        {
            get { return context.Books; }
        }

        public IQueryable<User> Users
        {
            get { return context.Users; }
        }

        public void SaveProduct(Book book)
        {
            if(book.BookID == 0)
            {
                context.Books.Add(book);
            }
            else
            {
                Book dbEntry = context.Books.Find(book.BookID);
                if(dbEntry != null)
                {
                    dbEntry.Name = book.Name;
                    dbEntry.Price = book.Price;
                    dbEntry.Description = book.Description;
                    dbEntry.Category = book.Category;
                    dbEntry.ImageData = book.ImageData;
                    dbEntry.ImageMimeType = book.ImageMimeType;
                }
            }
            context.SaveChanges();
        }

        public void SaveUser(User user)
        {
            if (user.UserId == 0)
            {
                context.Users.Add(user);
            }
            else
            {
                User dbEntry = context.Users.Find(user.UserId);
                if (dbEntry != null)
                {
                    dbEntry.Email = user.Email;
                    dbEntry.Password = user.Password;
                }
            }
            context.SaveChanges();
        }

        public Book DeleteProduct(int bookID)
        {
            Book dbEntry = context.Books.Find(bookID);
            if(dbEntry != null)
            {
                context.Books.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public User DeleteUser(int userID)
        {
            User dbEntry = context.Users.Find(userID);
            if (dbEntry != null)
            {
                context.Users.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

    }
}
