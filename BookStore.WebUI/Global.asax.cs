﻿using BookStore.WebUI.App_Start;
using BookStore.WebUI.Model;
using BookStore.WebUI.Infrastructure;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;

namespace BookStore.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new BookDbInitializer());
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
            FilterConfig.RegisterGlobalFilter(GlobalFilters.Filters);
        }
    }
}
