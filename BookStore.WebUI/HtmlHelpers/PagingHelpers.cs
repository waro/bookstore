﻿using BookStore.WebUI.Model;
using System;
using System.Text;
using System.Web.Mvc;

namespace BookStore.WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(
            this HtmlHelper html,
            PagingInfo pagingInfo,
            Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();

            int startValue = GetNumberFirstPage(pagingInfo);
            int endValue = GetNumberLastPage(pagingInfo) ;

            for (int i = startValue; i <= endValue; i++)
            {
                TagBuilder tagLi = new TagBuilder("li");
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                {
                    tagLi.AddCssClass("active");
                }
                tagLi.InnerHtml = tag.ToString();
                result.Append(tagLi.ToString());

            }
            return MvcHtmlString.Create(result.ToString());
        }

        private static int GetNumberFirstPage(PagingInfo pagingInfo)
        {
            if (pagingInfo.CurrentPage == 1)
            {
                return pagingInfo.CurrentPage;
            }

            return (pagingInfo.CurrentPage == pagingInfo.TotalPages 
                    && pagingInfo.TotalPages != 2) ?
                    pagingInfo.CurrentPage - 2 : pagingInfo.CurrentPage - 1;
        }

        private static int GetNumberLastPage(PagingInfo pagingInfo)
        {
            if (pagingInfo.CurrentPage == 1)
            {
                return (pagingInfo.CurrentPage + 1 >= pagingInfo.TotalPages) ?
                    pagingInfo.TotalPages : pagingInfo.CurrentPage + 2;
            }
            
            return (pagingInfo.CurrentPage + 1 > pagingInfo.TotalPages) ?
                    pagingInfo.TotalPages : pagingInfo.CurrentPage + 1;
        }

    }
}