﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.WebUI.Model
{
    public class CategoryModel
    {
        public List<Category> CategoryList { get; set; }
        public string RangeSlider { get; set; }
        public int PageSizeSelected { get; set; }
        public string SortTypeSelected { get; set; }
    }

    public class Category
    {
        public bool CheckedStatus { get; set; }
        public string CategoryName { get; set; }
    }
}