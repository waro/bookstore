﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using BookStore.Domain.Concrete;
using BookStore.Domain.Entities;

namespace BookStore.WebUI.Model
{
    public class BookDbInitializer : DropCreateDatabaseAlways<EFDbContext>
    {
        protected override void Seed(EFDbContext context)
        {
            EFDbContext db = new EFDbContext();

            User defaultUser = new User
            {
                Email = "email@email.com",
                Password = "svkLRj9nYEgZo7gWDJD5IQ==",   //password => 123
                Name = "Валерий Мережковский",
            };
            db.Users.Add(defaultUser);

            Book[] defaultBooks = new Book[]
            {
                    new Book { Category = "History", Description = "1942",
                        Name = "War", Price = 25 },
                    new Book { Category = "History", Description = "1943",
                        Name = "War", Price = 30 },
                    new Book { Category = "History", Description = "1944",
                        Name = "War", Price = 20 },
                    new Book { Category = "History", Description = "1945",
                        Name = "War", Price = 45 },
                    new Book { Category = "Fantastic", Description = "2025",
                        Name = "Future", Price = 25 },
                    new Book { Category = "Fantastic", Description = "2035",
                        Name = "Future", Price = 35 },
                    new Book { Category = "Fantastic", Description = "2045",
                        Name = "Future", Price = 45 },
                    new Book { Category = "Fantastic", Description = "2075",
                        Name = "Future", Price = 15 },
                    new Book { Category = "ASP.NET", Description = "MVC",
                        Name = "A.Freeman", Price = 25 },
                    new Book { Category = "ASP.NET", Description = "MVC",
                        Name = "A.Freeman", Price = 45 },
                    new Book { Category = "History", Description = "1942",
                        Name = "War", Price = 25 },
                    new Book { Category = "History", Description = "1943",
                        Name = "War", Price = 30 },
                    new Book { Category = "History", Description = "1944",
                        Name = "War", Price = 20 },
                    new Book { Category = "History", Description = "1945",
                        Name = "War", Price = 45 },
                    new Book { Category = "Fantastic", Description = "2025",
                        Name = "Future", Price = 25 },
                    new Book { Category = "Fantastic", Description = "2035",
                        Name = "Future", Price = 35 },
                    new Book { Category = "Fantastic", Description = "2045",
                        Name = "Future", Price = 45 },
                    new Book { Category = "Fantastic", Description = "2075",
                        Name = "Future", Price = 15 },
                    new Book { Category = "ASP.NET", Description = "MVC",
                        Name = "A.Freeman", Price = 25 },
                    new Book { Category = "ASP.NET", Description = "MVC",
                        Name = "A.Freeman", Price = 45 },
            };

            foreach (var item in defaultBooks)
            {
                db.Books.Add(item);
            }

            db.SaveChanges();

            base.Seed(context);
        }
    }
}