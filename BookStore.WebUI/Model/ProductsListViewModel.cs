﻿using BookStore.Domain.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BookStore.WebUI.Model
{
    public class ProductsListViewModel
    {
        public IEnumerable<Book> Products { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public CategoryModel CategoryModel { get; set; }
        public SelectList PageSizeList { get; set; }
        public SelectList SortTypeList { get; set; }
    }
}