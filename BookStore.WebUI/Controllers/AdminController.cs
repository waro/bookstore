﻿using BookStore.Domain.Abstract;
using BookStore.Domain.Entities;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        // GET: Admin
        private IProductRepository _repository;
        public AdminController(IProductRepository repo)
        {
            _repository = repo;
        }
        public ViewResult Index()
        {
            return View(_repository.Books);
        }

        [HttpGet]
        public ViewResult Edit(int bookID)
        {
            return View(_repository.Books.FirstOrDefault(p => p.BookID == bookID));
        }

        [HttpPost]
        public ActionResult Edit(Book prod, HttpPostedFileBase image)
        {
            if(ModelState.IsValid)
            {
                if (image != null)
                {
                    prod.ImageMimeType = image.ContentType;
                    prod.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(prod.ImageData, 0, image.ContentLength);
                }
                _repository.SaveProduct(prod);
                TempData["message"] = string.Format("{0} has been saved", prod.Name);
                return RedirectToAction("Index");
            }
            else
            {
                return View(prod);
            }
        }
        
        [HttpGet]
        public ViewResult Create()
        {
            return View("Edit", new Book());
        }

        [HttpPost]
        public ActionResult Delete(int bookID)
        {
            Book deleteProduct = _repository.DeleteProduct(bookID);
            if(deleteProduct != null)
            {
                TempData["message"] = string.Format("{0} was deleted", deleteProduct.Name);
            }
            return RedirectToAction("Index");
        }


    }
}