﻿using BookStore.Domain.Abstract;
using BookStore.Domain.Entities;
using BookStore.WebUI.CustomCryptLibraries;
using System.Web.Mvc;
using System.Security.Claims;
using System.Web;
using System.Linq;



namespace BookStore.WebUI.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private IProductRepository _repository;
        public AuthController(IProductRepository repo)
        {
            _repository = repo;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var emailCheck = _repository.Users.FirstOrDefault(u => u.Email == model.Email);

            if (emailCheck == null)
            {
                ModelState.AddModelError("", "Invalid email or password");
                return View(model);
            }

            var getPassword = _repository.Users.Where(u => u.Email == model.Email).Select(u => u.Password);
            var materializePassword = getPassword.ToList();
            var password = materializePassword[0];
            var decryptedPassword = CustomDecrypt.Decrypt(password);

            if (model.Email != null && model.Password == decryptedPassword)
            {
                var getName = _repository.Users.Where(u => u.Email == model.Email).Select(u => u.Name);
                var materializeName = getName.ToList();
                var name = materializeName[0];

                var getEmail = _repository.Users.Where(u => u.Email == model.Email).Select(u => u.Email);
                var materializeEmail = getEmail.ToList();
                var email = materializeEmail[0];

                var identity = new ClaimsIdentity(new[]
                {
                        new Claim(ClaimTypes.Name, name),
                        new Claim(ClaimTypes.Email, email),
                    }, "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                bool check = identity.IsAuthenticated;

                return RedirectToAction("List", "Product");
            }

            ModelState.AddModelError("", "Invalid email or password");

            return View(model);
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("List", "Product");
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registration(User model)
        {
            if (ModelState.IsValid)
            {
                var queryUser = _repository.Users.FirstOrDefault(u => u.Email == model.Email);

                if (queryUser == null)
                {
                    var encryptedPassword = CustomEncrypt.Encrypt(model.Password);
                    var user = new User();
                    user.Email = model.Email;
                    user.Password = encryptedPassword;
                    user.Name = model.Name;
                    _repository.SaveUser(user);
                }
                else
                {
                    return RedirectToAction("Registration");
                }
            }
            else
            {
                ModelState.AddModelError("", "Incorrect email or password");
            }
            return View();
        }

        public PartialViewResult UserName()
        {
            string name = "Гость";

            if (Request.IsAuthenticated)
            {
                name = User.Identity.Name;
            }

            return PartialView((object)name);
        }
    }
}