﻿using BookStore.Domain.Abstract;
using System.Linq;
using System.Web.Mvc;
using BookStore.Domain.Entities;
using BookStore.WebUI.Model;
using System.Collections.Generic;

namespace BookStore.WebUI.Controllers
{
    [AllowAnonymous]
    public class ProductController : Controller
    {
        public int PageSize = 15;
        CategoryModel categoryDefaultModel = new CategoryModel();
        private IProductRepository _repository;
        public ProductController(IProductRepository productRepository)
        {
            _repository = productRepository;
            categoryDefaultModel.CategoryList = new List<Category>();
            categoryDefaultModel.CategoryList = BindCategory();
        }


        public ViewResult List(CategoryModel categoryModel, int page = 1)
        { 

            ProductsListViewModel model = GetProductListViewModel(categoryModel, page);

            return View(model);
        }

        public PartialViewResult Paging(CategoryModel categoryModel, int page = 1)
        {
            ProductsListViewModel model = GetProductListViewModel(categoryModel, page);

            return PartialView(model);
        }

        private List<int> GetPriceRange(string str)
        {
            var priceRange = str.Split(new char[] { ',' })
                    .Select(p => int.Parse(p)).ToList();

            ViewBag.CurrentRange = "[" + priceRange[0].ToString() + "," + priceRange[1].ToString() + "]";

            return priceRange;
        }

        private ProductsListViewModel GetProductListViewModel(CategoryModel categoryModel, int page)
        {
            decimal minPrice = 0;
            decimal maxPrice = 0;
            ViewBag.CurrentRange = "[0, 99]";

            if (categoryModel.RangeSlider != null)
            {
                var priceRange = GetPriceRange(categoryModel.RangeSlider);
                minPrice = priceRange[0];
                maxPrice = priceRange[1];
            }
            List<string> checkedCategory = GetCheckedCategory(categoryModel);

            var query = _repository.Books
                .Where(p => (checkedCategory.Contains(p.Category) || !checkedCategory.Any()));

            if (minPrice > 0)
            {
                query = query.Where(p => p.Price > minPrice);
            }

            if (maxPrice > 0)
            {
                query = query.Where(p => p.Price < maxPrice);
            }

            if (categoryModel.PageSizeSelected != 0)
            {
                PageSize = categoryModel.PageSizeSelected;
            }

            var querySort = query.OrderBy(p => p.Price);

            if (categoryModel.SortTypeSelected == "Дорогие")
            {
                querySort = query.OrderByDescending(p => p.Price);
            }

            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = querySort
                .Skip((page - 1) * PageSize)
                .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemPerPage = PageSize,
                    TotalItem = query.Count()
                },
                SortTypeList = new SelectList(new List<string>
                {
                    "Дешевые",
                    "Дорогие",
                }),
                CategoryModel = categoryModel,
                PageSizeList = new SelectList(new List<int>() {
                    15,
                    30,
                    45,
                })
            };
            return model;
        }

        [HttpGet]
        public FileContentResult GetImage(int bookID)
        {
            Book prod = _repository.Books.FirstOrDefault(p => p.BookID == bookID);
            if (prod != null)
            {
                return File(prod.ImageData, prod.ImageMimeType);
            }
            else
            {
                return null;
            }
        }

        private List<string> GetCheckedCategory(CategoryModel categoryModel)
        {

            CategoryModel result = categoryDefaultModel;
            if (categoryModel.CategoryList != null)
            {
                result = categoryModel;
            }
            return result.CategoryList
            .Where(p => p.CheckedStatus == true)
            .Select(p => p.CategoryName)
            .ToList();

        }

        public ActionResult FilterByCategory(CategoryModel model)
        {
            ViewBag.CurrentRange = "[0, 99]";
            if (model.CategoryList != null)
            {

                return PartialView(model);
            }
            return PartialView(categoryDefaultModel);
        }

        public List<Category> BindCategory()
        {
            List<Category> objcountry = new List<Category>();
            IEnumerable<string> categories = _repository.Books
            .Select(x => x.Category)
            .Distinct()
            .OrderBy(x => x);

            foreach (var item in categories)
            {
                objcountry.Add(new Category { CategoryName = item });
            }

            return objcountry;
        }
    }
}